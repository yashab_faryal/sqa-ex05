Feature: Catalog examination
Narrative:
In order to inventory the available products
As a shop owner
I want to examine my catalog of products

Scenario: an item in catalog has no subcategories
Given my catalog has category Home
When I list its subcategories
Then they should be empty

Scenario: an category in catalog has products
Given my catalog has category Home
When I list the products in this category
Then products should be non empty

Scenario: info. of an existing product can be accessed
Given that Hoppin' Hot Sauce exists
When I search for the product
Then I should get all its information

Scenario: attribute of an existing product is valid
Given that Hoppin' Hot Sauce exists
When I search for its attributes
Then I should get attribute heatRange with value 2