Feature: Adding customer attributes
Narrative:
In order to manage customer information
As a shop owner
I want to add and remove attributes of customers

Scenario: an attribute age is to be updated for a customer
Given customer attribute age exists
When the attribute age is given a new value
Then the value is updated

Scenario: the attribute age is to be added for a customer
Given customer attribute age does not exist
When the attribute age and the value is added
Then the attribute age exists for the customer

Scenario: the attribute age needs to be deleted for the customer
Given customer attribute age exists
When I delete the age attribute for the customer
Then attribute age does not exist

Scenario: all attributes of a customer is deleted
Given that one or more attributes exist for a customer
When I delete all attributes for a customer
Then customer does not have any attributes