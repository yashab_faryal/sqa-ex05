package userstory2;

import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import org.apache.commons.httpclient.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserStory2Steps {
    private final String url = "http://localhost:8082/api/v1/";
    HttpClient client = new HttpClient();
    private JSONArray attributes;
    private JSONObject ageAttribute;
    private String attributeName;

    @Given("customer attribute age exists")
    public void hasAttributeAge() {
        GetMethod method = new GetMethod(url + "customer");
        try {
            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }
            attributes = (JSONArray) new JSONObject(method.getResponseBodyAsString()).get("customerAttributes");
        } catch (HttpException e) {
            System.err.println("Fatal protocol violation: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Fatal transport error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
    }

    @When("the attribute age is given a new value")
    public void updateValue() throws IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPut httpPut = new HttpPut(url + "customer/attribute");
        httpPut.setHeader("Accept", "application/json");
        httpPut.setHeader("Content-type", "application/json");
        String json = "  \"age\": \"24\"\r\n" + "}";
        ResponseHandler<String> responseHandler = response -> {
            int status = response.getStatusLine().getStatusCode();
            if (status >= 200 && status < 300) {
                HttpEntity entity = response.getEntity();
                return entity != null ? EntityUtils.toString(entity) : null;
            } else {
                throw new ClientProtocolException("Unexpected response status: " + status);
            }

        };
        String responseBody = httpclient.execute(httpPut, responseHandler);
    }

    @Then("the value is updated")
    public void shouldHaveAttribute() {
        assertTrue(attributes.length() == 1);
    }

    @Given("customer attribute age does not exist")
    public void newAttribute() {
        GetMethod method = new GetMethod(url + "customer");
        try {
            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }
            attributes = (JSONArray) new JSONObject(method.getResponseBodyAsString()).get("customerAttributes");
        } catch (HttpException e) {
            System.err.println("Fatal protocol violation: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Fatal transport error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
    }

    @When("the attribute age and the value is added")
    public void addAttribute() throws IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPut httpPut = new HttpPut(url + "customer/attribute");
        httpPut.setHeader("Accept", "application/json");
        httpPut.setHeader("Content-type", "application/json");
        String json = "  \"age\": \"24\"\r\n" + "}";
        ResponseHandler<String> responseHandler = response -> {
            int status = response.getStatusLine().getStatusCode();
            if (status >= 200 && status < 300) {
                HttpEntity entity = response.getEntity();
                return entity != null ? EntityUtils.toString(entity) : null;
            } else {
                throw new ClientProtocolException("Unexpected response status: " + status);
            }

        };
        String responseBody = httpclient.execute(httpPut, responseHandler);
    }

    @Then("the attribute age exists for the customer")
    public void shouldHaveNewAttribute() {
        assertTrue(attributes.length() == 1);
    }

    @When("I delete the age attribute for the customer")
    public void deleteAgeValue() throws IOException {
        attributeName = "age";
        URL theUrl = new URL(url + String.format("customer/attribute/%s", attributeName));
        HttpURLConnection httpCon = (HttpURLConnection) theUrl.openConnection();
        httpCon.setDoOutput(true);
        httpCon.setRequestProperty(
                "Content-Type", "application/x-www-form-urlencoded");
        httpCon.setRequestMethod("DELETE");
        httpCon.connect();
    }

    @Then("attribute age does not exist")
    public void noAttributeFound() {
        assertTrue(attributes.length() == 0);
    }

    @Given("that one or more attributes exist for a customer")
    public void hasOneOrMoreAttributes() {
        GetMethod method = new GetMethod(url + "customer");
        try {
            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }
            attributes = (JSONArray) new JSONObject(method.getResponseBodyAsString()).get("customerAttributes");
        } catch (HttpException e) {
            System.err.println("Fatal protocol violation: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Fatal transport error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
    }

    @When("I delete all attributes for a customer")
    public void deleteAllAttributes() throws IOException {
        URL theUrl = new URL(url + "customer/attribute");
        HttpURLConnection httpCon = (HttpURLConnection) theUrl.openConnection();
        httpCon.setDoOutput(true);
        httpCon.setRequestProperty(
                "Content-Type", "application/x-www-form-urlencoded");
        httpCon.setRequestMethod("DELETE");
        httpCon.connect();
    }

    @Then("customer does not have any attributes")
    public void shouldBeEmpty() {
        assertTrue(attributes.length() == 0);
    }


}
