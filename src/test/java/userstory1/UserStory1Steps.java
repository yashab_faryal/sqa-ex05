package userstory1;

import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.client.utils.URIBuilder;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import org.apache.commons.httpclient.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserStory1Steps {
    private final String url = "http://localhost:8082/api/v1/";
    HttpClient client = new HttpClient();
    private Integer categoryId;
    private Integer productId;
    private JSONObject product;
    private JSONArray categories;
    private JSONArray products;
    private JSONArray attributes;

    @Given("my catalog has category Home")
    public void hasCategoryHome() {
        GetMethod method = new GetMethod(url + "catalog/category?searchParameter=Home&productLimit=1");
        try {
            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }
            categoryId = (Integer) new JSONObject(method.getResponseBodyAsString()).get("id");
        } catch (HttpException e) {
            System.err.println("Fatal protocol violation: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Fatal transport error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
    }

    @When("I list its subcategories")
    public void listSubcategories() {
        GetMethod method = new GetMethod(url + String.format("catalog/category/%d/categories", categoryId));
        try {
            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }
            categories = (JSONArray) new JSONObject(method.getResponseBodyAsString()).get("categories");
        } catch (HttpException e) {
            System.err.println("Fatal protocol violation: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Fatal transport error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
    }

    @Then("they should be empty")
    public void shouldBeEmpty() {
        assertTrue(categories.length() == 0);
    }

    @When("I list the products in this category")
    public void listProductsInCategory() {
        GetMethod method = new GetMethod(url + String.format("catalog/category/%d?productLimit=1", categoryId));
        try {
            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }
            products = (JSONArray) new JSONObject(method.getResponseBodyAsString()).get("products");
        } catch (HttpException e) {
            System.err.println("Fatal protocol violation: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Fatal transport error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
    }

    @Then("products should be non empty")
    public void productsNonEmpty() {
        assertTrue(products.length() > 0);
    }

    @Given("that Hoppin' Hot Sauce exists")
    public void aProductExists() {
        listProductsInCategory();
        productId = (Integer) products.getJSONObject(0).get("id");
    }

    @When("I search for the product")
    public void searchProduct() {
        GetMethod method = new GetMethod(url + String.format("catalog/product/%d", productId));
        try {
            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }
            product = new JSONObject(method.getResponseBodyAsString());
        } catch (HttpException e) {
            System.err.println("Fatal protocol violation: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Fatal transport error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
    }

    @Then("I should get all its information")
    public void getAllProductInformation() {
        assertTrue(product.get("name").equals("Hoppin' Hot Sauce"));
    }

    @When("I search for its attributes")
    public void searchProductAttributes() {
        GetMethod method = new GetMethod(url + String.format("catalog/product/%d/attributes", productId));
        try {
            int statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                System.err.println("Method failed: " + method.getStatusLine());
            }
            attributes = new JSONArray(method.getResponseBodyAsString());
        } catch (HttpException e) {
            System.err.println("Fatal protocol violation: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Fatal transport error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
    }

    @Then("I should get attribute heatRange with value 2")
    public void heatRangeShouldBe2() {
        assertTrue(attributes.getJSONObject(0).get("attributeName").equals("heatRange"));
        assertTrue(attributes.getJSONObject(0).get("attributeValue").equals("2"));
    }
}
